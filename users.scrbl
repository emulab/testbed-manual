#lang scribble/manual
@(require "defs.rkt")

@title[#:tag "users" #:version apt-version]{@(tb) Users}

Registering for an account is @seclink["register"]{quick and easy}. Registering
doesn't cost anything, it's simply for accountability. 
@not-powder{
We just ask that if
you're going to use @(tb) for anything other than light use, you tell us a bit
more about who you are and what you want to use @(tb) for.
}
@powder-only{
Using @(tb) is free for NSF funded academics. Short term use of the platform by other users (non-NSF funded academic use, industry or government use etc.), for the purpose of evaluating the platform, is also free. Use of the platform beyond such short-term evaluation will require payment. We expect to have rates, payment mechanisms etc., in place in the near future.
}

Users in @(tb) are grouped into @seclink["projects"]{@italic{projects}}: a
project is a (loosely-defined) group of people working together on some common
goal, whether that be a research project, a class, etc. @(tb) places a lot of
trust on project leaders, including the ability to authorize others to use the
@(tb). We therefore require that project leaders be faculty, senior research
staff, or others who are relatively senior positions.

@section[#:tag "register"]{Register for an Account}

To get an account on @(tb), you either @seclink["join-project"]{join an existing
project} or @seclink["create-project"]{create a new one}. In general, if you are
a student, you should join a project led by a faculty member with whom
you're working.

If you already have an account on
@hyperlink["http://www.emulab.net/"]{Emulab.net}, you don't need to sign
up for a new account on @(tb)---simply log in with your Emulab username and
password.

@subsection[#:tag "join-project"]{Join an existing project}

@screenshot["join-project.png"]

To join an existing project, simply use the ``Sign Up'' button found on every
@(tb) page. The form will ask you a few basic questions about yourself and the
institution you're affiliated with.

An SSH public key is required; if you're
unfamiliar with creating and using ssh keypairs, we recommend taking a look
at the first few steps in
@hyperlink["https://help.github.com/articles/generating-ssh-keys"]{GitHub's
guide to generating SSH keys}. (Obviously, the steps about how to upload the
keypair into GitHub don't apply to @(tb).)

@margin-note{@(tb) will send you email to confirm your address---watch for it (it
might end up in your spam folder), as your request won't be processed until
you've confirmed your address.}
You'll be asked to enter the project ID for the project you are asking to
join; you should get this from the leader of the project, likely your advisor
or your class instructor. (If they don't already have a project on @(tb), you
can @seclink["create-project"]{ask them to create one}.) The leader of your
project is responsible for approving your account.


@subsection[#:tag "create-project"]{Create a new project}

@screenshot["create-project.png"]

@margin-note{You should only start a new project if you are a faculty member,
senior research staff, or in some other senior position.  Students should ask
their advisor or course instructor to create a new project.}

To start a new project, use the ``Sign Up'' button found on every @(tb) page. In
addition to basic information about yourself, the form will ask you a few
questions about how you intend to use @(tb). The application will be reviewed
by our staff, so please provide enough information for us to understand the
research or educational value of your project. The review process may take
a few days, and you will receive mail informing you of the outcome.

Every person working in your project needs to have
@seclink["join-project"]{their own account}. You get to approve these
additional users yourself (you will receive email when anyone applies to join.)
It is common, for example, for a faculty member to create a project which is
primarily used by his or her students, who are the ones who run experiments. We
still require that the project leader be the faculty member, as we require that
there is someone in a position of authority we can contact if there are
questions about the activities of the project.

Note that projects in @(tb) are publicly-listed: a page that allows users to
see a list of all projects and search through them does not exist yet, but
it will in the future.

@;{

@subsection[#:tag "invite-project"]{Invite others to join your project}

@TODO{Write about this when the feature is finished}

}

@subsection[#:tag "ssh-access"]{Setting up SSH access}

The first step to setting up ssh access to @(tb) is to generate an ssh key pair.

**For Windows users:**

A good option for a Windows ssh client is to make use of putty:

@hyperlink["https://www.ssh.com/ssh/putty"]{https://www.ssh.com/ssh/putty}


To generate and manage ssh keys with putty:

@hyperlink["https://www.ssh.com/ssh/putty/windows/puttygen"]{https://www.ssh.com/ssh/putty/windows/puttygen}

**For Linux and Mac OS users:**

To generate and manage ssh keys:

@hyperlink["https://www.ssh.com/ssh/keygen/"]{https://www.ssh.com/ssh/keygen/}

Once you have generated an ssh key pair, you need to upload the public key into the @(tb) portal:

Log into the @(tb) portal. Once you are logged in: Click on your username (top right), select "Manage SSH keys" and follow the prompts to load the ssh public key.

The next time you instantiate an experiment, your ssh public key will be loaded onto all (ssh capable) nodes in your experiment, allowing direct access to these nodes using an ssh client.

@subsection[#:tag "x11"]{Setting up X11}

Experiment based graphical user interface (GUI) interaction on @(tb) often require  X windows, so you might find it useful to have an X-server running on the laptop/desktop you use to access resources on @(tb).

**For Windows users:**

Various options are available, for example:

@hyperlink["https://sourceforge.net/projects/vcxsrv/"]{https://sourceforge.net/projects/vcxsrv/}

**For Mac OS users:**

@hyperlink["https://www.xquartz.org"]{https://www.xquartz.org}

