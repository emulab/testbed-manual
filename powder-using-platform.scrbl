#lang scribble/manual

@(require "defs.rkt")

@title[#:tag "using" #:version apt-version]{Using @(tb): From simulation, to emulation, to over-the-air}

@(tb) is designed to allow users to start small and simple, and progress toward complex, real world outdoor operation (if that is the end goal of the work). We encourage users to step through the following environments toward this end:

@itemlist[
    @item{Apply and develop your custom changes using a profile that instantiates all required resources, e.g. a 5G Core, gNB and UE, using simulated RF. This only requires compute nodes, and RF behavior is 100% predictable and controllable. This make the development process simpler by eliminating unknowns caused by real radio behaviors and outside interference.}
    @item{Move your setup to one of @(tb)'s controlled RF environments, such as the conducted attenuator matrix or paired radio workbenches. In such controlled RF environments, the radio signals are real, but isolated from external influences. This makes it easier to debug and characterize your full communication and application stack. Further, with the attenuator matrix, RF path loss is able to be changed programatically.}
    @item{Transition to using the indoor over-the-air lab, where RF propagation is realistic, but within a short range, with limited external interference. This environment is a stepping stone toward outdoor operation, allowing for validation of your system without significant concerns related to link budget (available power), external interference, and similar issues that are present outdoors.}
    @item{Deploying onto real radio resources on the @(tb) outdoor radio environment. This is the ultimate test of a system, with real physical scale, external parties potentially creating interference, and dynamics such as moving objects and weather. In addition to fixed rooftop, side-of-road, and fixed, side-of-building (human height) radio deployments, @(tb)'s outdoor environment also includes mobile shuttle endpoints that can be utilized in your experiments. Experience shows that jumping straight to the outdoor environment can be a frustrating experience if you haven't already validated that your system works in more controlled settings first.}
]

The chapter sections that follow provide example scenarios and profiles for using each of the environments described above.

@section{Simulated RF Experimentation}

The @hyperlink["https://www.powderwireless.net/p/PowderProfiles/srsran5g-simulated-rf"]{srsran5g-simulated-rf} profile deploys an Open5GS 5G Core and srsRAN gNodeB/UE on a single compute node, with baseband IQ samples passed between the gNodeB and UE using ZMQ sockets. Optionally, you can also deploy a containerized version of the OSC RIC and run an example xApp that exercises the KPM service model using the ORAN resources provided by the SRS team.

The @hyperlink["https://www.powderwireless.net/p/mww2023/5g-sim"]{5G-sim} profile was developed for MWW2023 and we continue to maintain it because it is a very good start for using OAI 5G. Similar to the srsRAN profile mentioned above, this profile deploys an complete 5G network on a single node, with with baseband IQ samples passed via sockets. But it also takes advantage of OAI's channel modeling capabilities and UE/gNodeB soft scopes, which can be very useful for learning and debugging.

@section{Conducted RF Environment}

The @hyperlink["https://www.powderwireless.net/p/PowderTeam/srs-rf-matrix"]{srs-rf-matrix} profile deploys a complete 5G network with all radio antenna ports wired into our programmable @seclink["powder-cam-hw"]{conducted RF attenuator matrix}. This allows you to control the RF path loss between the gNodeB and UEs, introduce noise and interference, or observe uplink spectrum use. This environment is capable of emulating handover scenarios as well.

The @hyperlink["https://www.powderwireless.net/p/PowderTeam/oai-rf-bench"]{oai-rf-bench} profile deploys a complete 5G network in another @(tb) conducted RF environment, the @seclink["powder-prw-hw"]{paired radio workbenches}, which include two X310 SDRs with ports wired through 30 dB attenuators, and a common clock source for synchronization. This environment is ideal for testing and debugging your 5G network in a controlled RF environment if your application does not require the full flexibility of the conducted attenuator matrix.

@section{Using the Indoor Over-The-Air Lab}

@(tb) provides two example profiles for using the @seclink["powder-ota-hw"]{indoor over-the-air lab}:
@hyperlink["https://www.powderwireless.net/p/PowderTeam/oai-indoor-ota"]{oai-indoor-ota} and @hyperlink["https://www.powderwireless.net/p/PowderTeam/srs-indoor-ota"]{srs-indoor-ota}. Both of these deploy a complete 5G network using server class compute and an NI X310 SDR for the gNodeB, and up to four NUCs with COTS UEs. These profiles are often forked and extended to provide more complex network topologies and scenarios, e.g., multiple gNodeBs, interference sources, and more.

@section{Outdoor Over-The-Air Environment}

@;TODO{Old text/example below. Likely needs brushing up or replacement.}

The @(tb) over-the-air experimental workflow follows the same basic approach described in the @seclink["getting-started"]{getting started} chapter. However, over-the-air operation involves radio frequency (RF) transmissions, that are subject to FCC regulations. The implication of this is that over-the-air experiments cannot be instantiated on-demand. Rather all over-the-air experiments need to @seclink["reservations"]{pre-reserve} all resources that will be needed by the experiment, including spectrum, before the normal experiment instantiation process can be started.

In summary, the steps involved with instantiating an over-the-air experiment in @(tb) are:

@itemlist[
@item{Select a profile you want to instantiate and note the resources required from the profile description.}
@item{Request a reservation for the (sub-)set of resources you need.}
@item{Once your reservation is approved, follow the normal experimental workflow process to instantiate your experiment.}
]

@; Note: This process is expected to evolve as we gain more experience with over-the-air operation on @(tb).

@subsection{Step-by-step walkthrough}

The step-by-step walkthrough below will instantiate an over-the-air experiment using the @hyperlink["https://github.com/srsran/srsRAN_project"]{srsRAN Project} open source 5G software stack. The walkthrough assumes you have an account on @(tb) and have your own project that has been enabled by @(tb) administrators for over-the-air operation. (Specifically, you will not be able to use the "TryPowder" project to instantiate this profile.)

@itemlist[#:style 'ordered
	@instructionstep["Log into the portal"
					#:screenshot "powder-website.png"]{
    	Start by pointing your browser at @url[(apturl)] and log in.
  	}

	@instructionstep["Find the resources needed by the srs-outdoor-ota profile"
					#:screenshot "powder-ota-profile.png"]{
		We will use the srsRAN Outdoor OTA profile developed by the @(tb) team, which you can find @hyperlink["https://www.powderwireless.net/p/PowderProfiles/ota_srslte"]{here}. Alternatively you can search for the profile: Select "Experiments" and then "Start Experiment" from the portal. Then select "Change Profile". In the search box enter "srs-outdoor-ota" and select the "srsRAN 5G w/ Open5GS CN5G using the POWDER Dense Deployment" profile provided by the @(tb) team.
		
		In the profile description, note the hardware and frequency resources that are required for this profile.
		
		For this activity we will select the following specific resources:
		
		Hardware: ; Emulab, cnode-mario; Emulab, cnode-guesthouse; Emulab, d430 x2
		
		Frequency: 3430 MHz to 3450 MHz

		}

	@instructionstep["Go to the resource reservation page"
					#:screenshot "powder-reservation1.png"]{
		Within the @(tb) portal, select "Experiments", then select "Reserve Resources" from the drop down menu.
	}

	@instructionstep["Reserve the resources required by the srs-outdoor-ota profile"
					#:screenshot "powder-ota-res.png"]{
		Fill out the reservation page to reserve the specific resources identified above. Note that resource reservations are tied to specific projects. You will have to use your own project here, not the @(tb) team project ("PowderTeam") shown in the screenshot. (The "TryPowder" project used for the @seclink["getting-started"]{getting stared} activity also can not be used for over-the-air operation.)
		
		Select a date and time for your reservation and provide a "Reason" description.
		
		Once you have completed all the required fields, select "Check" to see if your reservation request can be accommodated. You might have to adjust your request (select different resources and/or change you reservation time/date) to make it fit. Note that the table/graphs on the right of the Reservation Request page show current frequency reservations and resource availability.

        In this example, we are reserving the mobile endpoints along with the other resources, which mean that the duration of the experiment will be limited to a single day. I.e., experiments that use the mobile endpoints can only run for a single day, since the campus shuttles go offline at night. However, it is sometimes desirable to create a separate longer-running experiment that includes everything except the mobile endpoints. For these cases, we provide a @hyperlink["https://www.powderwireless.net/p/PowderTeam/oai-indoor-ota"]{separate profile} for adding the mobile endpoints experiment. Of course, you'll still need to create as many day-long mobile endpoint reservations as you need to cover the duration of your srs-outdoor-ota experiment.
	}
	
	@instructionstep["Submit your reservation request" #:screenshot "powder-ota-subres.png"]{
		If your reservation request can be accommodated you will be presented with a pop-up window to "Submit" the reservation.
		
		Once you have submitted your reservation you will have to wait for the reservation to be approved by @(tb) administrators before proceeding with the rest of this activity (during the time slot requested in your request).   				
	}

	@instructionstep["Select the profile" #:screenshot "powder-ota-profile.png"]{
		You will receive email when your reservation request is "approved" and also when your reservation becomes "active".
		
		When your reservation becomes active you can proceed to instantiate the ota_srslte profile. (Alternatively, you can follow the steps below as soon as your reservation is approved, but in the "Schedule" step, schedule the instantiation of the profile to coincide with your reservation.)
		
		As before, you can find the "srs-outdoor-ota" profile @hyperlink["https://www.powderwireless.net/p/PowderTeam/srs-outdoor-ota"]{here}. Alternatively you can search for the profile: Select "Experiments" and then "Start Experiment" from the portal. Then select "Change Profile". In the search box enter "srs-outdoor-ota" and select the select the "srsRAN 5G w/ Open5GS CN5G using the POWDER Dense Deployment" profile provided by the @(tb) team.
	}

	@instructionstep["Select resources for your profile" #:screenshot "powder-ota-parameter.png"]{
		In the "Parameterize" step, select the @bold{same} resources you requested/received approval for, and adjust the other parameters as needed for your experiment (e.g., uncheck the "Include mobile endpoints..." box if using the separate profile for the mobile endpoints).
		
	}
	
	@instructionstep["Finalize" #:screenshot "powder-ota-finalize.png"]{
		In the "Finalize" step, be sure to select the same project under which you submitted the resource reservation request.				
	}

	@instructionstep["Schedule" #:screenshot "powder-ota-useres.png"]{
		In the "Schedule" step, select a start/end time that fits in your reservation period, or auto populate the start/end times by checking the box next to your reservation. Then select "Finish" to instantiate your experiment.
	}

	@instructionstep["Wait..." #:screenshot "powder-ota-swap.png"]{
	    It will take several minutes to instantiate the experiment, during which the experiment status page will be updated in realtime as your resources come on line.
	}

	@instructionstep["Experiment becomes ready" #:screenshot "powder-ota-ready.png"]{
        Once your experiment is fully instantiated, i.e., shows "Your experiment is ready!" and all of the "Startup" column values in the "List View" tab say "Finished", your network is deployed, the gNodeBs are started (since we checked the box to automatically deploy them in the parameterization step), and the COTS UEs should begin to attach to your gNodeBs as the campus shuttles pass by them.
	}

	@instructionstep["Look at the Powder Map tab" #:screenshot "powder-ota-map.png"]{
        The UEs won't necessarily attach immediately, or on every pass, but they should eventually and periodically attach if the buses are running on routes that pass by the gNodeB sites you selected. You can see the a map that includes the locations of the dense sites and the campus shuttles in your experiment if you click on the "Powder Map" tab on the experiment status page.
	}

	@instructionstep["Look at the Grafana dashboard" #:screenshot "powder-ota-grafana.png"]{
		The srs-outdoor-ota profile includes a Grafana dashboard that shows some logging outputs gathered from the various core network and RAN components in your experiment and stored to the "orch" node using @hyperlink["https://grafana.com/docs/loki/latest/send-data/promtail/"]{Promtail} and @hyperlink["https://github.com/grafana/loki"]{Loki}. The profile instructions includes a link to the dashboard and the login credentials generated for this specific experiment. After some time you should see COTS UE data like RSRP/RSRQ/SINR per shuttle location start to show up. The image included here shows the logs for a single shuttle over a two hour period.
	}

]

@subsection{Some Important Notes}
@itemlist[
    @item{If you want to use spectrum other than the profile default (3430-3450 MHz at the time of writing), you must not enable the "Automatically start the srsRAN gNodeBs" option in the parameterization step, since you will first need to update the srsRAN gNodeB configuration files (at @tt{/var/tmp/etc/srsran/gnb_rf_b200_tdd_n78_20mhz.yml} on each dense site node) to reflect the appropriate center frequency (in ARFCN format) and bandwidth. After this, you can start the gNodeBs manually by running @tt{sudo systemctl restart srs-gnb} on each dense site node.}
    @item{The srs-outdoor-ota and mobile-endpoints profiles include a handy TUI utility for interacting directly with the UEs. You can use this to, e.g., check the serving cell info or toggle airplane mode. To run this utility, ssh into the bus in question (assuming you've already @seclink["ssh-access"]{added your ssh pub key} to your @(tb) account), and run @tt{/var/tmp/ue_app.py
	}.}
	@item{If the UEs aren't attaching at all to the gNodeBs they pass by, there may be some bad state or configuration at the UE. Post on the @seclink["getting-help"]{@(tb) forum} with a link to your current experiment and we will help sort it out.}
	@item{You can find further detail by, and a lot more can be learned from, looking at the srs-outdoor-ota profile's git repository linked from the profile page.}
]

@section{Next Steps}

All of the profiles mentioned in this section, and other profiles provided by the @(tb) team in general, are meant to serve as jumping off points for your own experimentation and research. We encourage you to fork and modify these profiles to suit your own needs. As such, all of the profiles mentioned above build the RAN components from source, and include the source/builds in place so you can make modifications straight away. Some example modifications:

@itemlist[
	@item{Modifying the MAC scheduler for an srsRAN/OAI gNodeB to test a new scheduling algorithm.}
	@item{Modifying an srsRAN/OAI gNodeB to expose a metric or KPI that is not currently available.}
	@item{Adapting the Promtail/Loki/Grafana logging setup to include additional metrics, or further parse data from the configured logs to create a data set for training AI/ML models.}
	@item{Adding additional network services, such as a RIC or xApp.}
	@item{Adding near-edge compute nodes to the network to host additional services, such as a video transcoder or AI/ML model inference engine.}
]

If you have any questions or need help, please don't hesitate to ask on the @(tb) forum.
