#!/bin/sh

IMG=$1
TMP=$1.tmp.png
OLD=$1.old.png

magick $IMG -bordercolor white -border 30 \( +clone -background black -shadow 80x8+8 \) +swap -background white -layers merge +repage $TMP && mv $IMG $OLD && mv $TMP $IMG
