#lang scribble/manual
@(require racket/date)
@(require "defs.rkt")

@title[#:version apt-version
       #:date (date->string (current-date))]{The POWDER Manual}

@author[
    "The Powder Team"
]

@;{
@italic[(if (equal? (doc-mode) 'pdf)
    (list "The HTML version of this manual is available at " (hyperlink apt-doc-url apt-doc-url))
    (list "This manual is also available as a " (hyperlink "http://docs.powderwiresless.net/manual.pdf" "PDF")))]
}

Powder is a facility for experimenting on the future of wireless
networking in a city-scale "living laboratory" environment. It was one
of the first two platforms funded by the Platforms for Advanced
Wireless Research program at the National Science Foundation (NSF),
receiving support from NSF grants
@hyperlink["https://www.nsf.gov/awardsearch/showAward?AWD_ID=1827940"]{CNS-1827940} and
@hyperlink["https://www.nsf.gov/awardsearch/showAward?AWD_ID=2346555"]{CNS-2346555}.

Powder is developed, deployed, and operated by the University of Utah, in partnership with the @hyperlink["https://renew.rice.edu/"]{RENEW} team at Rice University.

The Powder facility is built on top of
@hyperlink["http://www.emulab.net/"]{Emulab} and is run by the
@hyperlink["http://www.flux.utah.edu"]{Flux Research Group}, part of the
@hyperlink["http://www.cs.utah.edu/"]{Kahlert School of Computing} at the
@hyperlink["http://www.utah.edu/"]{University of Utah}.

@table-of-contents[]

@include-section["powder-intro.scrbl"]
@include-section["powder-getting-started.scrbl"]
@include-section["powder-profiles.scrbl"]
@include-section["users.scrbl"]
@include-section["repeatable-research.scrbl"]
@include-section["basic-concepts.scrbl"]
@include-section["creating-profiles.scrbl"]
@include-section["powder-spectrum.scrbl"]
@include-section["reservations.scrbl"]
@include-section["powder-using-platform.scrbl"]
@include-section["geni-lib.scrbl"]
@include-section["advanced-storage.scrbl"]
@include-section["advanced-topics.scrbl"]
@include-section["virtual-machines.scrbl"]
@include-section["powder-hardware.scrbl"]
@include-section["powder-tutorial-srs.scrbl"]
@include-section["powder-tutorial.scrbl"]
@include-section["openstack-tutorial.scrbl"]
@include-section["cite.scrbl"]
@include-section["getting-help.scrbl"]
