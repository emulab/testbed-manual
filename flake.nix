{
  description = "Build environment for testbed manuals";

  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/nixpkgs-unstable";

    geni-lib = {
      url = "git+https://gitlab.flux.utah.edu/emulab/geni-lib.git";
      flake = false;
    };
  };

  outputs = { self, nixpkgs, geni-lib }:
  with import nixpkgs { system = "x86_64-linux"; };
  let
    geni-lib-package = python312.pkgs.buildPythonPackage {
      pname = "geni-lib";
      version = "0.1";
      src = geni-lib;

      propagatedBuildInputs = with python312.pkgs; [ cryptography lxml requests wrapt ];

      outputs = [
        "out"
        "doc"
      ];

      nativeBuildInputs = [
        sphinxHook
      ];

      buildInputs = [ python312.pkgs.sphinx_rtd_theme python312.pkgs.six ];

      # Currently, the tests in geni-lib need to be updated for python3
      doCheck = false;

      meta = {
        homepage = "https://gitlab.flux.utah.edu/emulab/geni-lib";
        description = "GENI library";
      };
    };
    python-with-my-packages = 
          (python312.withPackages (python-packages: [
            python-packages.six
            python-packages.sphinx
            python-packages.sphinx_rtd_theme
            geni-lib-package]));
    GENI_COMPILED_DOC_DIR = "${geni-lib-package.doc}/share/doc/python3.12-geni-lib-0.1/html/";
    buildInputs = [
      git
      racket
      python-with-my-packages
      geni-lib-package.doc
    ]; in
    {
      devShell.x86_64-linux = mkShell {
            inherit GENI_COMPILED_DOC_DIR buildInputs;
      };
      defaultPackage.x86_64-linux =
        stdenv.mkDerivation {
           pname = "CloudLab Manual";
           version = "1.0";
           src = self;
           inherit GENI_COMPILED_DOC_DIR buildInputs;
        
           # No method to install via this, yet
           dontInstall = true;
      };
    };
}
