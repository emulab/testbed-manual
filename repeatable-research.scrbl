#lang scribble/manual
@(require "defs.rkt")

@title[#:tag "repeatable-research" #:version apt-version]{@(tb) and Repeatable Research}

One of @(tb)'s key goals is to enable @italic{repeatable research}---we aim to
make it easier for researchers to get the same software and hardware
environment so that they can repeat or build upon each others' work.

@apt-only{

    @seclink["virtual-machines"]{Virtual machines} in @(tb) do @bold{not}
    provide strong resource guarantees or performance isolation from other
    concurrent users. Therefore, they are suitable primarily under the
    following conditions:

    @itemlist[
        @item{During initial exploration, development, gathering of preliminary
            performance results, etc.}
        @item{When it is the output of the software, rather than the software's
        performance, that is of interest.}
    ]

    We therefore recommend that researchers who want to provide a repeatable
    environment do the following:

    @itemlist[#:style 'ordered
        @item{Conduct initial development and gather initial numbers using
            @seclink["virtual-machines"]{virtual machines}. Because much of the
            time in this phase is often spent on debugging, development, etc.,
            using a full physical machine is often an inefficient use of
            resources.}
        @item{Switch to @seclink["physical-machines"]{physical machines} to
            collect numbers of publication. This ensures that published numbers
            are not affected by interference from other users of @(tb).}
    ]

    Similarly, for those who are repeating or building on the work of others,
    we recommend:

    @itemlist[#:style 'ordered
        @item{During the initial, exploratory phase of figuring out how to run
            the code, examining its configuration and parameters, etc., use
            @seclink["virtual-machines"]{virtual machines}.}
        @item{Switch to @seclink["physical-machines"]{physical machines} when
            it's time to do real experiments, compare performance with
            published results, etc.}
    ]

    @future-work["planned-virt-switching"]

    Because the @seclink["disk-images"]{disk images} that @(tb) provides boot
    on both virtual and physical machines, in most cases, switching means
    simply modifying one's profile to request the other type of resource.
}

@clab-only{

    @(tb) is designed as a @italic{scientific instrument}. It gives full
    visibility into every aspect of the facility, and it's designed to minimize
    the impact that simultaneous slices have on each other. This means that
    researchers using @(tb) can fully understand why their systems behave the
    way they do, and can have confidence that the results that they gather are
    not artifacts of competition for shared hardware resources. @(tb)
    @seclink["profiles"]{profiles} can also be published, giving other
    researchers the exact same environment—hardware and software—on which to
    repeat experiments and compare results.

    @(tb) gives exclusive access to compute resources to one experiment at
    a time. (That experiment may involve re-exporting those resources to
    other users, for example, by running cloud services.) Storage resources
    attached to them (eg. local disk) are also used by a single experiment
    at a time, and it is possible to run experiments that have 
    exclusive access to switches.

}

@powder-only{
	@(tb) is designed as a @italic{scientific instrument}. It gives full
    visibility into every aspect of the facility, and it's designed to minimize
    the impact that simultaneous users have on each other. This means that
    researchers using @(tb) can fully understand why their systems behave the
    way they do, and can have confidence that the results that they gather are
    not artifacts of competition for shared hardware resources.
    
    @(tb) @seclink["profiles"]{profiles} can also be published, giving other
    researchers the exact same environment—hardware and software—on which to
    repeat experiments and compare results.

    By default @(tb) gives exclusive access to specific frequency resources to one experiment at a time. 
}

@clab-only{

@section[#:tag "aec"]{@(tb) For Artifact Evaluation}

@(tb) is an ideal environment for artifact evaluation: it provides a way for
authors to define an environment in which they know their experiments will work,
and a way for AEC members to get access to large collections of computing hardware
without having to own that hardware themselves or pay for a commercial cloud
environment. 

In this section, we offer advice to authors of artifacts, AEC chairs, and the members
of AECs. 

@subsection[#:tag "aec-authors"]{For Authors}

The best way to give evaluators a predictable environment is to provide a link to
a @seclink["profiles"]{profile} that is suitable for running your artifact. This could
be one of our standard profiles, or one that you @seclink["creating-profiles"]{make
yourself}. Either way, you can be sure of the environment they will get so that you
do not have to worry about hardware differences, software versions, etc. This profile
may be simple, giving them access to a single machine with a default OS, or may have
multiple machines and/or use features such as @seclink["disk-images"]{disk images} and
@seclink["geni-lib-example-os-install-scripts"]{startup scripts} to manage dependencies
and configuration so that the evaluators have minimal work to do.

On the page for each profile, there is a "share" button: this will provide you with
the link you can give to evaluators. Because profiles are
@seclink["versioned-profiles"]{versioned}, you can provide them with a link to either
a specific version of the profile or to the profile in general (which will always point
to the latest version). If you are using a profile that you made yourself, we recommend
that you set the profile to be publicly available, though it is also possible to share
private profiles (these use a link with a random identifier; anyone with the link can use
the profile).

Evaluators will need to have a @(tb) account to use your profile. We do @italic{not}
recommend having them join your project (as this removes their anonymity) or giving
them access to a running experiment (as this can be unnecessarily complex, and requires
your experiment to remain instantiated for a potentially long period of time). Instead,
we recommend that evaluators use accounts obtained as described below in the 
@seclink["aec-chairs"]{"AEC Chairs"} and @seclink["aec-members"]{"AEC Members"} sections.
You may point them to this manual to explain the process if they are not familiar with 
it.

We recommend using a profile that specifies a specific @seclink["hardware"]{hardware type}
so that you can ensure that evaluators are running on machines with the same type of
performance, the same amount of memory, etc. We also recommend that you use a profile
that specifies an explicit @seclink["disk-images"]{disk image(s)} to use, even if that
image is one of @(tb)'s default images. This way, even if @(tb)'s default changes, you
can be assured that evaluators will continue to get the same one you used and will
not run into any unexpected software version problems.

Because many artifacts are shared as @tt{git} repositories,
@seclink["repo-based-profiles"]{repo-based profiles} can be a good way to package the
profile with the rest of the artifact. In essence, any public @tt{git} repo can be made
into a profile by adding a @tt{profile.py} script (which defines the profile using
@seclink["geni-lib"]) to it. If you chose to do this, you will still provide the evaluators
with a link to a profile to use: it will just be the case that the definition of this
profile is contained within the same repository as the rest of your artifact. For an
example of this, see the artifact repository for the
@hyperlink["https://github.com/ordersage/paper-artifact"]{OrderSage project}.

@subsection[#:tag "aec-chairs"]{For AEC Chairs}

@(tb) can be a useful resource for AECs as a place with significant resources
to run artifacts. It can be useful whether or not artifacts are submitted as @(tb)
profiles (as described @seclink["aec-authors"]{above}): AEC members can get
machines with a variety of @seclink["hardware"]{hardware} and popular OSes, and
they have root access, so installing dependencies, etc. is straightforward. Many 
members of certain research communities will already have experience using @(tb).

We recommend that AEC chairs @seclink["create-project"]{start a project} for the AEC;
the @(tb) staff will review your project application, so make sure it is clear about
which venue it is an AEC for. Once the project is approved, you will receive
links that you can share with AEC members to @seclink["join-project"]{join the project}.
Project leads approve members of their projects, so @(tb) staff will not be involved
further in the process.

@(tb) can sometimes be quite busy, and resources (especially hardware with particularly
valuable properties) can fill up. We recommend that you warn your AEC members to
plan ahead and leave themselves plenty of time to make sure they can get the resources
they need. The @seclink["reservations"]{reservation system} can be used to help get
access to scarce resources.

@subsection[#:tag "aec-members"]{For AEC Members}

@(tb) can be a useful resource for AEC members whether or not artifacts are submitted
as @(tb) profiles (as described @seclink["aec-authors"]{above}): AEC members can get
machines with a variety of @seclink["hardware"]{hardware} and popular OSes, and
they have root access, so installing dependencies, etc. is straightforward.

We recommend that AEC members @seclink["join-project"]{join a project} created
specifically for the AEC. If your AEC chairs have not provided you with information
about a @(tb) project for the AEC, we recommend that you point them to the
@seclink["aec-chairs"]{section above} for instructions to create one.

If you already have an account on @(tb), it is possible to be a member of multiple
projects with the same account; simply log in to your existing account before applying
to join the new project.

Since AECs frequently work on tight deadlines, and @(tb) has limited resources, we 
recommend planning ahead to make sure that you have enough time to obtain the resources
you need and do the evaluation. If you are having trouble with the resources you need
being unavailable, have a look at @(tb)'s @seclink["reservations"]{reservation system}.

}
