"""An example of a Docker container running a standard, augmented system image."""

import geni.portal as portal
import geni.rspec.pg as rspec

request = portal.context.makeRequestRSpec()
node = request.DockerContainer("node")
node.disk_image = "urn:publicid:IDN+emulab.net+image+emulab-ops//docker-ubuntu16-std"
portal.context.printRequestRSpec()
