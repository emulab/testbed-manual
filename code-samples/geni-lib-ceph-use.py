request = pc.makeRequestRSpec()
...
node = request.RawPC("node")

# make this project's CephFS filesystem accessible
node.mountCephFS()

...
