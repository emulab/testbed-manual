"""An example of constructing a profile with 20 Docker containers in a LAN,
divided across two container hosts.

Instructions: Wait for the profile instance to start, and then log in to
the container via the ssh port specified below.  By default, your
container will run a standard Ubuntu image with the Emulab software
preinstalled.
"""

import geni.portal as portal
import geni.rspec.pg as rspec
# Import the Emulab specific extensions.
import geni.rspec.emulab as emulab

# Create a Request object to start building the RSpec.
request = portal.context.makeRequestRSpec()

# Create a LAN to put containers into.
lan = request.LAN("lan")

# Create two container hosts, each with ten Docker containers.
for j in range(0,2):
    # Create a container host.
    host = request.RawPC("host-%d" % (j))
    # Select a specific hardware type for the container host.
    host.hardware_type = "d430"

    for i in range(0,10):
        # Create a container.
        node = request.DockerContainer("node-%d-%d" % (j,i))
        # Create an interface.
        iface = node.addInterface("if1")
        # Add the interface to the LAN.
        lan.addInterface(iface)
        # Set this container to be instantiated on the host created in
        # the outer loop.
        node.InstantiateOn(host.client_id)

# Print the RSpec to the enclosing page.
portal.context.printRequestRSpec()
