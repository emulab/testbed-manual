"""An example of an image backed dataset. The dataset name and mountpoint can be customized when you instantiate the profile.

Instructions:
Log into your node, your dataset filesystem is in the directory you specified during profile instantiation."""

# Import the Portal object.
import geni.portal as portal
# Import the ProtoGENI library.
import geni.rspec.pg as pg
# Import the emulab extensions library.
import geni.rspec.emulab

# Create a portal context, needed to defined parameters
pc = portal.Context()

# Create a Request object to start building the RSpec.
request = pc.makeRequestRSpec()

pc.defineParameter("DATASET", "URN of your image-backed dataset", 
                   portal.ParameterType.STRING,
                   "urn:publicid:IDN+emulab.net:testbed+imdataset+pgimdat")
pc.defineParameter("MPOINT", "Mountpoint for file system",
                   portal.ParameterType.STRING, "/mydata")

params = pc.bindParameters()

node = request.RawPC("mynode")
node.disk_image = 'urn:publicid:IDN+emulab.net+image+emulab-ops//UBUNTU16-64-STD'
bs = node.Blockstore("bs", params.MPOINT)
bs.dataset = params.DATASET

pc.printRequestRSpec(request)