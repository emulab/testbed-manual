"""An example of constructing a profile with ten Docker containers in a LAN.

Instructions: Wait for the profile instance to start, and then log in to
the container via the ssh port specified below.  By default, your
container will run a standard Ubuntu image with the Emulab software
preinstalled.
"""

import geni.portal as portal
import geni.rspec.pg as rspec

# Create a Request object to start building the RSpec.
request = portal.context.makeRequestRSpec()

# Create a LAN to put containers into.
lan = request.LAN("lan")

# Create ten Docker containers.
for i in range(0,10):
    node = request.DockerContainer("node-%d" % (i))
    # Create an interface.
    iface = node.addInterface("if1")
    # Add the interface to the LAN.
    lan.addInterface(iface)

# Print the RSpec to the enclosing page.
portal.context.printRequestRSpec()
