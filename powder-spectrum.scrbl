#lang scribble/manual

@(require "defs.rkt")

@title[#:tag "spectrum"]{Spectrum use}

Every @(tb) experiment must declare its spectrum usage in advance: 
while arbitrary reception is possible, transmission on any frequency is
permitted @bold{only by prior permission}.

Program Experimental licencees may submit experiment notifcations
on the FCC OET Experiments System site to operate within the
Salt Lake City Innovation Zone.  Details are available in the
@hyperlink["https://docs.fcc.gov/public/attachments/DA-19-923A1.pdf"]{Public
Notice announcing the Innovation Zone}.

The frequency bands appropriate for experimenter use varies by
node type; more details are available in @seclink["hardware"]{the
hardware chapter}.  The following table provides a summary of frequencies
for currently operational equipment:

@(tabular #:style 'boxed #:sep (hspace 3) (list
    (list "Frequency (GHz)" "Operational node types")
    (list @hyperlink["https://www.fcc.gov/wireless/bureau-divisions/broadband-division/advanced-wireless-services-aws"]{1.7/2.1 AWS (FDD)} (list "Base station " @tt{cellsdr} " nodes; Fixed endpoint " @tt{nuc} " and " @tt{ue} " nodes"))
    (list @hyperlink["https://www.fcc.gov/wireless/bureau-divisions/broadband-division/broadband-radio-service-education-broadband-service"]{2.4-2.6 ISM/BRS (TDD)} "Skylark Wireless Massive MIMO")
    (list @hyperlink["https://www.ntia.gov/files/ntia/publications/compendium/3300.00-3500.00_01DEC15.pdf"]{3.3-3.5 Radiolocation} (list "Base station " @tt{cbrssdr} " nodes"))
    (list @hyperlink["https://www.fcc.gov/wireless/bureau-divisions/mobility-division/35-ghz-band/35-ghz-band-overview"]{3.5 CBRS (TDD)} (list "Base station " @tt{cbrssdr} " nodes"))))

@;{
@section[#:tag "spectrum-brs-powder"]{BRS Channel Use Information}

@(tb) provides access to 10 MHz of spectrum (2496-2506 MHz) in the BRS range (band 41) for experimental use. Transmissions in this spectrum must adhere to a specific LTE or 5G-NR TDD frame and subframe configuration with downlink (rooftop base stations) and uplink (mobile/fixed endpoints) transmission periods synchronized in time with commercial operators in order to avoid interfering with their adjacent channels. Specifically, transmission from rooftop base stations is disallowed in the period between 1.64323 ms and 3 ms after the beginning of the second as determined by GPS* and repeating every 5 ms thereafter.

The required LTE uplink-downlink frame configuration is number 2 with special subframe configuration 6 (see 3GPP 36.211 and this @hyperlink["https://www.sqimway.com/lte_tdd.php"]{LTE TDD Reference}), which has this arrangement of downlink (D), uplink (U), and special (S) subframes: DSUDD. Each subframe has a duration of 1 ms and the pattern repeats every 5 ms. The special subframe in this pattern includes more time for downlink transmission (DwPTS), followed by a guard period (GP) to allow for time-of-flight differences for downlink reception, followed by more time for uplink transmission (UpPTS) prior to the next uplink subframe. Special subframe configuration 6 sets the durations for DwPTS, GP, and UpPTS as follows:

@itemlist[
@item{DwPTS: 0.64323 ms}
@item{GP:    0.21406 ms}
@item{UpPTS: 0.14271 ms}
]

@screenshot["powder-brs-tdd-config.png"]

For 5G-NR, DDDSUUDDDD with SF27 timing configured as 3:8:3 should be used. The special subframe consists of downlink for 107.14us, guard period for 272.71us and uplink for 107.14us.

* The radios on the rooftops use an Octoclock for their time reference. These Octoclocks are synchronized using WhiteRabbit to a GPS disciplined oscillator. The Octoclock is connected to the 10 MHz and PPS radio inputs. As such you should use the external frequency and time inputs for synchronization.  This is done with a @tt{set_time_source("external")} call via the @tt{multi_usrp} object API.
}

@section[#:tag "spectrum-monitoring"]{Radio monitoring}

To ensurce compliance with @hyperlink["https://www.ecfr.gov/current/title-47/chapter-I"]{FCC regulations} and @hyperlink["https://powderwireless.net/aup"]{@(tb) acceptable use conditions}, dedicated RF monitoring hardware continuously measures the operation of @(tb) radios and verifies that transmitters are being operated correctly.  @(tb) experiments will be shut down automatically if violations are detected.

If you have any questions about @(tb) compliance monitoring, please contact @hyperlink[(string-append "mailto:support@" (tb-domain))]{support@"@"@(tb-domain)} @emph{before} experimentation.

@subsection[#:tag "spectrum-monitoring-conducted"]{Conducted RF}

The @(tb) conducted RF hardware (specifically, the @seclink["powder-cam-hw"]{attenuator matrix nodes} and the @seclink["powder-prw-hw"]{paired radio workbench SDRs}) are not subject to RF monitoring, and @(tb) users are able to operate these radios at their discretion.  (Please note that the @hyperlink["https://powderwireless.net/aup"]{general @(tb) acceptable use policy} still applies.)

@subsection[#:tag "spectrum-monitoring-radiated"]{Radiated RF}

For other radio hardware (any transmit port connected to an antenna), @(tb) applies @hyperlink["https://arxiv.org/abs/2212.08179"]{an approach described by Wang et al.} and continually checks transmitter output against the frequency and power bounds specified in the experiment parameters.  If transmitter behaviour strays outside those bounds, the experiment will be shut down.  Therefore, it is essential to take proper care both when specifying the experiment as well as throughout its operation.

@subsection[#:tag "spectrum-monitoring-mmimo"]{Massive MIMO}

Conventional monitoring is not applied for the @hyperlink["https://wiki.renew-wireless.org/quickstartguide/userenewonpowder"]{RENEW massive MIMO} hardware, where special procedures apply.  Please consult @hyperlink[(string-append "mailto:support@" (tb-domain))]{support@"@"@(tb-domain)} for details about operating the massive MIMO arrays.
