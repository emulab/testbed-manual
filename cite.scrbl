#lang scribble/manual

@(require "defs.rkt")

@title[#:tag "cite" #:version apt-version]{Citing @(tb)}

If you use @(tb) in an academic publication, we ask that you refer to it by
name in your text, and that you cite the paper below: it helps us find papers
that have used the facility, helps readers know about the environment in which
your experiments were run, and helps us to report on the testbed's use to our
funders.

Thanks!

@clab-only{
    @verbatim|{
@inproceedings{Duplyakin+:ATC19,
    title     = "The Design and Operation of {CloudLab}",
    author    = "Dmitry Duplyakin and Robert Ricci and Aleksander Maricq and Gary Wong and Jonathon Duerig and Eric Eide and Leigh Stoller and Mike Hibler and David Johnson and Kirk Webb and Aditya Akella and Kuangching Wang and Glenn Ricart and Larry Landweber and Chip Elliott and Michael Zink and Emmanuel Cecchet and Snigdhaswin Kar and Prabodh Mishra",
    booktitle = "Proceedings of the {USENIX} Annual Technical Conference (ATC)",
    pages     = "1--14",
    year      = 2019,
    month     = jul,
    url       = "https://www.flux.utah.edu/paper/duplyakin-atc19"
}

    }|
}

@powder-only{
    @verbatim|{
@article{
Breen21POWDER,
Author = {Joe Breen and Andrew Buffmire and Jonathon Duerig and Kevin Dutt and Eric Eide and Anneswa Ghosh and Mike Hibler and David Johnson and Sneha Kumar Kasera and Earl Lewis and Dustin Maas and Caleb Martin and Alex Orange and Neal Patwari and Dan Reading and Robert Ricci and David Schurig and Leigh Stoller and Allison Todd and Jacobus (Kobus) Van der Merwe and Naren Viswanathan and Kirk Webb and Gary Wong},
Title = {{POWDER:} Platform for Open Wireless Data-driven Experimental Research},
Journal = {},
DOI = {10.1016/j.comnet.2021.108281},
Year = {2021}
}
    }|
}
