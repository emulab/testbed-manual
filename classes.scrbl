#lang scribble/manual
@(require "defs.rkt")

@title[#:tag "classes" #:version apt-version]{@(tb) for Classes}

@(tb) can be a good environment for classes: it provides students with separate,
isolated environments in which to do their work, and those environments can easily
be brought to a known clean state so that all students get a consistent environment.
Students get @tt{root} access on their nodes, making @(tb) ideal for classes that 
require configuration or code changes to the operating system, or low-level access
to the hardware. Because @(tb) is a network testbed, it is also idea for class 
projects that make use of multiple nodes.

There are some limitations that you should be aware of before deciding
to use @(tb) for a class:

@itemlist[
    @item{@(tb) has limited resources: in large classes, if students leave work to the
        last minute, or all students try to work on a deadline at the same time, they
        may run into problems getting the nodes they need in time.}

    @item{@(tb) has a policy that resources can only be held while they are actively
        being used. Make sure that students are aware that individual resource allocations
        are not meant to last all quarter/semester: students should start experiments
        when they are ready to begin work, and terminate them when they are done.}
]

You will want to make sure that students understand that storage on nodes in @(tb) is
@italic{ephemeral} and will be cleared out when experiments terminate. Addtionally, while
@(tb) makes every effort to provide a reliable service, hardware failures, etc. can
cause data loss. Students need to be sure to regularly copy off important work to
avoid loss due to experiment termination or equipment failures.

@seclink["reservations"]{Resource reservations} can be useful for classes: they ensure
that a certain number of resources are available for use by a project during a specified
time period. It's important to note that the resource pool is shared by all members of the
project: for example, if you reserve 10 nodes, they can be used by up to 10 different
experiments. Students do not need to do anything special to use these reservations: simply
starting an experiment during the reservation period will automatically make use of the
reservation.

If you have any questions about using @(tb) for classes, see the @seclink["getting-help"]{getting
help} chapter of this manual. We ask that instructors and TAs be the primary point of contact for
students in requesting help with @(tb): @(tb) has a small support staff that cannot handle a large
volume of student support.

@section[#:tag "creating-class-project"]{Creating Your Project on @(tb)}

Students should not apply for their own @seclink["projects"]{projects} on @(tb): the
instructor or an assistant (such as a TA) should @seclink["create-project"]{apply for a
project}. Be sure to check the box indicating that the project is for a class, and include
enough information in the application for us to have a look at the class, such as a link to
the syllabus if available.

We ask that you create a new project for each class that you teach; eg. each quarter/semester
of a course should apply for a new project on @(tb). If course staff (instructors, TAs, etc.) already
have an account on @(tb), they can re-use their existing accounts: simply log in before applying for the
new project.

Once the project has been approved, students will apply to join it as @seclink["joining-class-project"]{described
below}. Course staff can approve these accounts, without @(tb) staff having to get involved.
One way to smooth the process of approving accounts for large classes is to allow TAs to approve them; when approving a
new account, you may give it "manager" permissions, which allows the account to approve others. You may also
give users this status via the "Members" tab of the project page at any time.

@section[#:tag "joining-class-project"]{Having Students Join Your Project}

When a @(tb) project is approved, the project leader will receive an email with a link to join the project. This
link can be distributed to students in the course, who will use it to apply for an account. Once a student applies,
the project leader will receive email with the application, and can approve or deny it. Alternately, students can join
the project by following the standard @(tb) @seclink["join-project"]{"join an existing project"} instructions, and enter
the name of the class's project.

Students who apply to start their own project will have their project requests denied, and be asked to join the main
project for the class.

Students who already have an account on @(tb) can join the project without making a new account; simply log in before
applying to join the new project. 

@section[#:tag "project-classes"]{Project-Based Classes}

Classes that are based around students working independently or in groups are an ideal use case for @(tb); all
members of the project are given logins on all experiments started under the project, which enables easy
collaboration.

@section[#:tag "assignment-classes"]{Assignment-Based Classes}

For classes in which all students are working on the same task, it can be useful to either @seclink["profiles"]{use an
existing profile} or @seclink["creating-profiles"]{create a new one}. This way, all students will have the same environment,
simplifying instructions and making debugging easier. The page for each profile has a "share" button that can be used to
get a link to include with the assignment.

For large classes, we recommend considering whether @seclink["virtual-machines"]{virtual machines} can meet students'
needs, as this reduces the resource load on @(tb). Virtual machines should use @seclink["xen-shared-mode"]{shared mode}
so that they run on shared, not exclusive, hosts.

All members of a project are given shells on all experiments in the project: thus, TAs can log into nodes being used by
students to help examine or debug code.

