#lang scribble/manual

@(require "defs.rkt")

@title[#:tag "reservations" #:version apt-version]{Resource Reservations}

@(tb) supports @bold{reservations} that allow you to request resources ahead of
time. This can be useful for tutorials, classes, and to run larger experiments
than are typically possible on a first-come, first-served basis.
@powder-only{
For some @(tb) resources, such as certain over-the-air radios, a reservation is
required in order for experiment instantiation to be allowed. Further, since
many radio resources in POWDER (devices and spectrum) are scarce, it is
generally in the interest of users to make reservations so they can be
sure that the resources they need will be available when they are ready to
run experiments.
}

Reservations in @(tb) are @bold{per-cluster} and @bold{per-type}.  They are
tied to a @bold{project}: an experiment must belong to that project to
use the reserved nodes (in some cases, the scope of reservations might
be further restricted to experiments associated with a particular user,
or a particular subgroup). For homogeneous groups of resources (such as
compute nodes of the same type), reservations are not tied to specific nodes;
this gives @(tb) maximum flexibility to do late-binding of nodes to
experiments instantiated from reservations, which makes them minimally
intrusive on other users of the testbed.

@section[#:tag "reservations-guarantee"]{What Reservations Guarantee}

Having a reservation guarantees that, at minimum, the specified quantity of
nodes of the specified type will be available for use by the project during
the specified time window.

Having a reservation does @italic{not automatically start an experiment at that
time}: it ensures that the specified number of nodes are available for use by
any experiments that you (or in some cases, your fellow project members) start.

@italic{More than one experiment} may use nodes from the reservation; for
example, a tutorial in which 40 students each will run an experiment having a
single node may be handled as a single 40-node reservation. You may also start
and terminate multiple experiments in series over the course of the
reservation: reserved nodes will not be returned to general use until your
reservation ends.

A reservation guarantees the @italic{minimum} number of nodes that will be
available; you may use more so long as they are not used by other experiments
or reservations.

Experiments run during a reservation @italic{do not automatically terminate} at
the end of the reservation; they simply become subject to the normal resource
usage policies, and, for example, may become non-extendable due to other
reservations that start after yours.

Important caveats include:

@itemlist[
    @item{Nodes can take @italic{several minutes} to be freed and reloaded
    between experiments; this means that they may take a few minutes to be
    available at the beginning of your reservation, and if you terminate an
    experiment during your reservation, they may take a few minutes to be
    usable for your next experiment.}
    @item{The reservation system cannot account for factors outside of its
    control such as @italic{hardware failures}; this many result in occasional
    failures to get the full number of nodes in exceptional circumstances.}
    @item{The reservation system ensures that enough nodes of the specified type
    are available, but does not consider other factors such as network topology,
    and so @italic{cannot guarantee that all possible experiments can be
    started}, even if they fit within the number of nodes.}
]

@section[#:tag "reservations-affect"]{How Reservations May Affect You}

Reservations held by others may affect your experiments in two ways: they
may @bold{prevent you from creating new experiments} or may @bold{prevent you
from extending existing experiments}. This ``admission control system'' is how
we ensure that nodes are available for those that have them reserved.

If there is an ongoing or upcoming reservation by another project,  you may
encounter an ``admission control'' failure when trying to create a new
experiment. This means that, although there are enough nodes that are not
currently allocated to a particular experiment, some or all of those nodes
are required in order to fulfill a reservation. Note that the admission control
system assumes that your experiment will last for the full default experiment
duration when making this calcuation. For example, if the default experiment
duration is 24 hours, and a large reservation will start in 10 hours, your
experiment may fail to be created due to the admission control system. If the
large reservation starts in 30 hours, you will be able to create the
experiment, but you may not be able to extend it.

Reservations can also prevent you from extending existing experiments, if that
extension would cause too few nodes to be available to satisfy a reservation. 
A message will appear on the experiment's status page warning you when this
situation will occur in the near future, and the reservation request dialog
will limit the length of reservation that you can request. If this happens, be
sure to save all of your work, as the administrators cannot grant extensions
that would interfere with reservations.

@section[#:tag "reservations-making"]{Making a Reservation}

To request a reservation, use the ``Reserve Nodes'' item from the ``Experiments''
menu.

@screenshot["reservation-form.png"]

After filling out the number of and type of nodes and the time, the
@bold{check} button checks to see if the reservation is possible. If your
request is satisfiable, you will get a dialog box that lets you submit the
request.

@screenshot["reservation-submit.png"]

If your request is @italic{not} satisfiable, you will be given a chance to
modify the request and ``check'' again. In this case, the time when there will
not be enough nodes is shown, as will the number of nodes by which the request
exceeds availalbe resources. To make your reservation fit, try asking for a
different type of nodes, a smaller number, or a time further in the future.

Not all reservation requests are automatically accepted. Your request will be
shown as ``pending'' while it is being reviewed by the @(tb) administrators.
Requesting the smaller numbers of nodes, or for shorter periods of time, will
maximize the chances that youre request is accepted. Be sure to include
meaningful text in the ``Reason'' field, as administrators will use this to
determine whether to grant your reservation.

You may have more than one reservation at a time; if you need resources of
more than one type, or on different clusters, you can get this by requesting
mutliple reservations.

@section[#:tag "reservations-using"]{Using a Reservation}

To use a reservation, simply create experiments as normal. Experiments run
during the duration of the reservation (even those begun before its start
time) are automatically counted towards the reservation. Experiments run
during reservations have expiration times as do normal experiments, so be sure
to extend them if necessary.
@powder-only{
On @(tb), your projects' reservations will show up at the bottom of the
"Schedule" step. You can check the box next to them, which allows you to
set the duration of the experiment to coincide with the time window of
the selected reservation(s). If your reservation has already started,
then the start time will be left blank to indicate that the experiment
should start right away. If the reservation starts in the future, then
the experiment will be scheduled to instantiate at the start time of the
reservation.
}

Since reservations are associated with a project, if you belong to more than one, make sure
to create the experiment under the correct project.  (If the project is
organised into subgroups, selecting the appropriate subgroup might also
be necessary.)

@not-powder{
Experiments are not automatically terminated at the conclusion of a reservation
(though it may not be possible to extend them due to other reservations).
Remember to terminate your experiments when you are done with them, as you
would do normally.
}

If you finish your work, and still have any active experiments or related
reservations, we ask that you terminate the associated experiments and delete
the reservations so that the resources can be freed up for use by others.

@section[#:tag "reservations-shared"]{Who Shares Access to Reservations}

As @seclink["reservations"]{mentioned above}, every reservation is
tied to exactly one project, and experiments belonging to other projects
will be excluded from reserved resources for the duration of the
reservation.

Different projects can have very different organisational models, however.
One project might be run by a manager (such as a class instructor) who
makes reservations on behalf of the entire project, and then individual
project members start experiments each using a fraction of the reserved
resources.  Another project might operate informally, and its project
members behave independently: those members might prefer to make private
reservations for their own use (excluding their fellow project members
from intentionally or unintentionally consuming the reserved resources
with experiments of their own).

(Some projects are organised hierarchically, and make use of
@bold{subgroups} within the project; reservations can also be
handled by subgroup, if applicable.)

@(tb) offers three reservation sharing options a project may choose from:

@(tabular #:style 'boxed #:sep (hspace 3) (list
	(list "By user"
	      "An experiment will be able to access reserved resources only if the experiment and reservation are in the same project, and the experiment was started by the same user who requested the reservation.")
	(list "By project"
	      "An experiment will be able to access reserved resources only if the experiment and reservation are in the same project.")
	(list "By subgroup"
	      "An experiment will be able to access reserved resources only if the experiment and reservation are in the same project, and the experiment and the reservation specify the same subgroup.")))

@; Omit this section for now while we decide whether we want user AI for this.
@;{
The project leader may select one of those options on the "Project Info"
tab of the project page:

@; This is a temporary placeholder screenshot: replace it with a
@; screenshot of the real page once the final UI is available!
@screenshot["project-shared-reservations.png"]

Since modifying the reservation sharing semantics does affect the
interpretation of any current reservations within the project, it is
possible that a modification request might be denied because of
resource shortage; in that case, you will have to adjust the problematic
reservations and/or wait and retry the modification later.  The effect
of a successful modification is immediate, and the scope of any current
reservation will be expanded or constrained.  @(tb) will send
e-mail warning about such cases, but it is still @bold{the project
leader's responsibility} to communicate with project members, and
co-ordinate reservations as appropriate.
}

A project leader can request the reservation sharing semantics suitable
for their project by contacting
@hyperlink[(string-append "mailto:support@" (tb-domain))]{support@"@"@(tb-domain)}.

Please note that the sharing semantics of reservations must be chosen
(by the project leader) @bold{for the project as a whole}.  It is not
possible to request a custom scope for individual reservations.
