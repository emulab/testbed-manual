#lang scribble/manual
@(require "defs.rkt")

@title[#:tag "hardware" #:version apt-version]{Hardware and Wireless Environments}

As described in the @seclink["using"]{Using Powder} section,
@(tb) provides a wide variety of hardware and configured environments
for carrying out wireless research,
including conducted and over-the-air (OTA) RF resources as well as backend
computation resources.
These resources are spread across the University of Utah campus as shown
in the
@link["https://www.powderwireless.net/powder-map.php"]{Powder Map}.
Conducted RF resources are largely in the MEB datacenter, OTA resources
all across campus, "near edge" compute nodes in the MEB and Fort datacenters,
and additional "cloud" compute nodes in the Downtown datacenter (not shown
on the map).

Following is a brief description of these resources.
For further
information about the capabilities of, and restrictions on, the individual
OTA components, refer to the
@link["https://www.powderwireless.net/radioinfo.php"]{Radio Information page}
at the @(tb) portal.

@section[#:tag "powder-cam-hw"]{Conducted attenuator matrix resources}

A custom-built @link["https://www.jfwindustries.com/product-category/test-systems/attenuator-assemblies/19-inch-rack-50-ohm/"]{JFW Industries 50PA-960 attenuator matrix} allows for configurable
conducted RF paths between a variety of connected components.
While the actual RF paths are fixed, and must be manually configured by
@(tb) personnel, users can adjust the attenuation values on those paths
dynamically.

The @link["https://www.powderwireless.net/atten.php"]{current configuration of paths} is:

@screenshot["powder-attenuator-config.png"]

The connected components are:

@itemlist[
  @item{
    Two NI @link["https://www.ettus.com/all-products/usrp-n300/"]{N300} SDRs
    each with two 10Gb Ethernet connections to the @(tb) wired network.
    The @(tb) nodes names are @tt{n300-1} and @tt{n300-2}.
  }
  @item{
    One NI @link["https://www.ettus.com/all-products/x310-kit/"]{X310} SDR
    with two @link["https://www.ettus.com/all-products/ubx160/"]{UBX160} daughter boards
    and two 10Gb Ethernet connections to the @(tb) wired network.
    The @(tb) node name is @tt{x310-1}.
  }
  @item{
    Eleven NI @link["https://www.ettus.com/all-products/ub210-kit/"]{B210} SDRs
    each with a USB3 connection to an Intel NUC.
    The NUC provides basic compute capability and a 1Gb Ethernet connection
    to the @(tb) wired network. The majority of the NUCs
    (@tt{nuc1} to @tt{nuc4}, @tt{nuc8} to @tt{nuc10}, @tt{nuc16}) are:

    @(nodetype "nuc5300" 8 "Maple Canyon, 2 cores"
        (list "CPU"   "Core i5-53000U processor (2 cores, 2.3Ghz)")
	(list "RAM"   "16GB Memory (2 x 8GB DDR3 DIMMs)")
	(list "Disks" "128GB SATA 6Gbps SSD Drive")
	(list "NIC"   "1GbE embedded NIC")
    )

    The remaining NUCs (@tt{nuc22}, @tt{nuc23}, @tt{nuc27}) are newer,
    with up to four cores and 32GB of RAM.
  }
]

The N300 and X310 SDRs have their 1 PPS and 10 MHz clock inputs connected
to an NI
@link["https://www.ettus.com/all-products/octoclock-g/"]{Octoclock-G}
module to provide a synchronized timing base. Currently the Octoclock is
@italic{not} synchronized to GPS.
The B210 SDRs are @italic{not} connected to the Octoclock.

@section[#:tag "powder-prw-hw"]{Paired Radio workbench resources}

The Paired Radio workbench consists of two sets of two
NI @link["https://www.ettus.com/all-products/x310-kit/"]{X310} SDRs
each, cross connected via SMA cables with a fixed 30 dB attenuation on
each path. Connections are shown in this diagram:

@screenshot["powder-paired-config.png"]

One pair, @tt{oai-wb-a1} and @tt{oai-wb-a2}, has a single
NI @link["https://www.ettus.com/all-products/ubx160/"]{UBX160} daughter board
along with a single 10Gb Ethernet port connected to the @(tb) wired network.
The second pair, @tt{oai-wb-b1} and @tt{oai-wb-b2}, has two
NI @link["https://www.ettus.com/all-products/ubx160/"]{UBX160} daughter boards
and two 10Gb Ethernet ports connected to the @(tb) wired network.

All four X310 SDRs have their 1 PPS and 10 MHz clock inputs connected to an NI
@link["https://www.ettus.com/all-products/octoclock-g/"]{Octoclock-G}
module to provide a synchronized timing base. Currently the Octoclock is
@italic{not} synchronized to GPS.

@section[#:tag "powder-ota-hw"]{Indoor OTA lab resources}

The Indoor OTA environment consists of four
NI @link["https://www.ettus.com/all-products/ub210-kit/"]{B210} SDRs
with antennas on one side of the lab, and four
NI @link["https://www.ettus.com/all-products/x310-kit/"]{X310} SDRs
with antennas on the opposite side of the lab:

@screenshot["powder-ota-config.png"]

The four B210 devices are each connected to a four-port
@link["https://www.taoglas.com/datasheets/MA963.A.BIVW.002.pdf"]{Taoglas MA963}
antenna in a 2x2 MIMO configuration, meaning that the TX/RX and
RX2 ports on both Channel A and Channel B are connected to an antenna
element. The B210 port to antenna port connectivity is as follows: Channel
A TX/RX to port 1; Channel A RX2 to port 2; Channel B RX2 to port 3;
Channel B TX/RX to port 4.

The four B210 devices are accessed via USB from their associated Intel NUC
nodes, @tt{ota-nuc1} to @tt{ota-nuc4}. All NUCs are of the same type:

    @(nodetype "nuc8559" 4 "Coffee Lake, 4 cores"
        (list "CPU"   "Core i7-8559U processor (4 cores, 2.7Ghz)")
	(list "RAM"   "32GB Memory (2 x 16GB DDR4 DIMMs)")
	(list "Disks" "500GB NVMe SSD Drive")
	(list "NIC"   "1GbE embedded NIC")
    )

Also connected to each of the NUCs is a
@link["https://www.quectel.com/product/5g-rm520n-series/"]{RM520N COTS UE}
connected to a four-port
@link["https://www.taoglas.com/datasheets/MA963.A.BIVW.002.pdf"]{Taoglas MA963}
antenna. Allocating the associated NUC will provide access to the COTS UE.

On the other side of the room, two of the X310 SDRs,
@tt{ota-x310-2} and @tt{ota-x310-3},
also have 2x2 MIMO capability via
@link["https://cdn.taoglas.com/datasheets/GSA.8841.A.105111.pdf"]{Taoglas GSA.8841 I-bar}
antennas connected to each of their Channel 0, and Channel 1 TX/RX and
RX2 ports.
The other two X310s, @tt{ota-x310-1} and @tt{ota-x310-4} have two of the
I-bar antennas each, connected to the TX/RX and RX2 ports of channel 0 only.

All radios (X310s and B210s) have their 1 PPS and 10 MHz clock
inputs connected to 
@link["https://www.ettus.com/all-products/octoclock-g/"]{Octoclock-G}
modules to provide a synchronized timing base.
The X310 devices connect to one Octoclock, and the B210 devices
connect to another, with the two Octoclocks interconnected.
Currently the Octoclocks are @italic{not} synchronized to GPS.

@section[#:tag "powder-bs-hw"]{Rooftop Base-station resources}

@(tb) currently has seven rooftop base stations around the main University
of Utah campus, as highlighted on this map:

@screenshot["powder-bs-map.png"]

(For a better sense of scale and location relative to all resources, see the
@link["https://www.powderwireless.net/powder-map.php"]{Powder Map}.)
The @tt{siteid}s (used for node naming) for each base station are
(clockwise from top right):
Hospital (@tt{hospital}), Honors (@tt{honors}), Behavioral (@tt{bes}),
Friendship (@tt{fm}), Browning (@tt{browning}), MEB (@tt{meb}), and
USTAR (@tt{ustar}).

Each @(tb) base station site includes a climate-controlled enclosure
containing radio devices connected to Commscope and Keysight antennas.
Each device has one or more dedicated 10G links to aggregation switches at
the Fort Douglas datacenter.  These network connections can be flexibly
paired with compute at the aggregation point or slightly further
upstream with Emulab/CloudLab resources.

Allocatable devices at base stations include:
@itemlist[
  @item{
    One NI @link["https://www.ettus.com/all-products/x310-kit/"]{X310} SDR
    with a
    @link["https://www.ettus.com/all-products/ubx160/"]{UBX160} daughter board
    connected to a
    @link["https://www.commscope.com/catalog/antennas/product_details.aspx?id=69448"]{Commscope VVSSP-360S-F}
    multi-band antenna.
    The TX/RX port of the X310 is connected to one of the 3200-3800 MHz ports
    on the Commscope antenna. The radio can operate in TDD mode,
    transmit only, or receive only using that port.
    The X310 has two 10Gb Ethernet ports connected to the @(tb) wired network.
    These SDRs are available at all rooftops with the @(tb) node name:
    @tt{cbrssdr1-siteid}, where @italic{siteid} is one of:
    @tt{bes}, @tt{browning}, @tt{fm}, @tt{honors}, @tt{hospital}, @tt{meb},
    or @tt{ustar}.
  }
  @item{
    One NI @link["https://www.ettus.com/all-products/x310-kit/"]{X310} SDR
    with a single
    @link["https://www.ettus.com/all-products/ubx160/"]{UBX160} daughter board
    connected to a
    @link["https://www.keysight.com/us/en/assets/7018-05040/data-sheets/5992-1237.pdf"]{Keysight N6850A}
    broadband omnidirectional antenna.
    The X310 RX2 port is connected to the wideband Keysight antenna and is
    receive-only.
    The X310 has a single 10Gb Ethernet
    port connected to the @(tb) wired network.
    These SDRs are only available at a subset of the base stations with the
    @(tb) node names @tt{cellsdr1-siteid}, where @italic{siteid} is one of:
    @tt{hospital}, @tt{bes}, or @tt{browning}.
    @bold{NOTE:} The other rooftop sites do @italic{not} have this connection
    due to isolation issues with nearby commercial cellular transmitters
    (too much power that would damage the receiver).
  }
  @item{Skylark Wireless Massive MIMO Equipment.
    @bold{NOTE:} Please refer to the
    @link["https://wiki.renew-wireless.org/quickstartguide/userenewonpowder"]{Massive MIMO on POWDER}
    documentation for complete and up-to-date information on using the
    massive MIMO equipment.

    Briefly, Skylark equipment consists of chains of multiple radios connected
    through a central hub.  This hub is 64x64 capable, and has 4 x 10 Gb
    Ethernet backhaul connectivity.  @(tb) provides compute and RAM intensive
    Dell R840 nodes (see compute section below) for pairing with the
    massive MIMO equipment.

    Massive MIMO deployments are available at the Honors, USTAR, and MEB
    sites (@(tb) names: @tt{mmimo1-honors}, @tt{mmimo1-meb}, and
    @tt{mmimo1-ustar}).
  }
]

In addition to these user-allocatable resources, each enclosure contains
several infrastructure devices of note:
@itemlist[
  @item{
    A Safran (formerly Seven Solutions)
    @link["https://safran-navigation-timing.com/product/white-rabbit-len/"]{WR-LEN}
    endpoint. Part of a common White Rabbit time distribution network used to
    synchronize radios at all rooftop and dense base station sites.
  }
  @item{
    An NI
    @link["https://www.ettus.com/all-products/octoclock-g/"]{Octoclock-G}
    module to provide a synchronized timing base to all SDRs. The Octoclock
    is synchronized with all other base stations and GPS via the White Rabbit
    network.
  }
  @item{
    A low-powered Intel NUC for environmental, connectivity, and RF monitoring
    and management.
  }
  @item{
    A Dell 1Gb network switch for connecting devices inside the enclosure.
  }
  @item{
    An fs.com passive CWDM mux for multiplexing multiple 1Gb/10Gb connections
    over a single fiber-pair backhaul to the Fort datacenter.
  }
  @item{
    A managed PDU to allow remote power control of devices inside the
    enclosure.
  }
]

@section[#:tag "powder-dbs-hw"]{Dense-deployment Base-station resources}

@(tb) currently has five street-side and one rooftop base station located
in a dense configuration around one common shuttle route on the main
University of Utah campus. The base stations are highlighted in red and
the shuttle route shown in green on this map:

@screenshot["powder-dbs-map.png"]

(For a better sense of scale and location relative to all resources, see the
@link["https://www.powderwireless.net/powder-map.php"]{Powder Map}.)
The @tt{siteid}s) (used for node naming) for each base station are
(clockwise from upper left):
NC Wasatch (@tt{wasatch}), NC Mario (@tt{mario}), Moran (@tt{moran}),
Guest House (@tt{guesthouse}), EBC (@tt{ebc}), and USTAR (@tt{ustar}).

@(tb) dense deployment base station locations contain one or more radio
devices connected to a Commscope antenna and a small compute node connected
via fiber to the Fort Douglas aggregation point.

Allocatable devices at dense base stations include:
@itemlist[
  @item{
    NI @link["https://www.ettus.com/all-products/ub210-kit/"]{B210} SDR
    connected to a
    @link["https://www.commscope.com/catalog/antennas/product_details.aspx?id=69448"]{Commscope VVSSP-360S-F}
    multi-band antenna.
    The Channel 'A' TX/RX port is connected to a CBRS port of the antenna.
    The B210 is accessed via a USB3 connection to a
    @link["https://www.onlogic.com/nuvo-7501/"]{Neousys Nuvo-7501}
    ruggedized compute node:

    @(nodetype "nuvo7501" 1 "Coffee Lake, 4 cores"
       (list "CPU"   "Intel Core i3-8100T (4 cores, 3.1GHz}")
       (list "RAM"   "32GB wide-temperature range RAM")
       (list "Disks" "512GB wide-temperature range SATA SSD storage")
	(list "NIC"   "1GbE embedded NIC")
    )

    The compute node is connected via two 1Gbps connections to a local switch
    which in turn uplinks via 10Gb fiber to the Fort datacenter.

    The @(tb) compute node name is @tt{cnode-siteid}, where @italic{siteid}
    is one of: @tt{ebc}, @tt{guesthouse}, @tt{mario}, @tt{moran}, @tt{ustar},
    or @tt{wasatch}.
  }
  @item{
    Benetel RU-650.
  }
]

In addition to these user-allocatable resources, each enclosure contains
several infrastructure devices of note:
@itemlist[
  @item{
    A Safran (formerly Seven Solutions)
    @link["https://safran-navigation-timing.com/product/white-rabbit-len/"]{WR-LEN}
    endpoint. Part of a common White Rabbit time distribution network used to
    synchronize radios at all rooftop and dense base station sites.
  }
  @item{
    Locally designed/built RF frontend/switch.
  }
  @item{
    A 1Gb/10Gb network switch for connecting devices inside the enclosure.
  }
  @item{
    An fs.com passive CWDM mux for multiplexing multiple 1Gb/10Gb connections
    over a single fiber-pair backhaul to the Fort datacenter.
  }
  @item{
    A managed PDU to allow remote power control of devices inside the
    enclosure.
  }
]

@section[#:tag "powder-fe-hw"]{Fixed-endpoint resources}

@(tb) has ten "fixed endpoint" (FE) installations which are enclosures
permanently affixed to the sides of buildings at roughly human height (5-6ft).
The endpoints are highlighted in red on the following map:

@screenshot["powder-fe-map.png"]

(For a better sense of scale and location relative to all resources, see the
@link["https://www.powderwireless.net/powder-map.php"]{Powder Map}.)
The @tt{siteid}s) (used for node naming) for each FE are
(clockwise from top right):
Moran (@tt{moran}), EBC (@tt{ebc}), Guest House (@tt{guesthouse}),
Sage Point (@tt{sagepoint}), Madsen (@tt{madsen}),
Central Parking Garage (@tt{cpg}), Law 73 (@tt{law73}),
Bookstore (@tt{bookstore}), Humanities (@tt{humanities}), and WEB (@tt{web}).

Each @(tb) FE enclosure contains an ensemble of radio equipment with
complementary small form factor compute nodes. Unlike base stations,
FEs do not have fiber backhaul connectivity. Instead they use commercial
cellular and Campus WiFi to provide seamless access to resources.

There are three allocatable devices at fixed endpoints:
@itemlist[
  @item{
    One Quectel
    @link["https://www.quectel.com/product/5g-rm520n-series/"]{RM520N COTS UE}
    connected to four
    @link["https://cdn.taoglas.com/datasheets/GSA.8841.A.105111.pdf"]{Taoglas GSA.8841 I-bar}
    antennas.
    The UE is USB3 connected to a NUC to provide basic compute capability
    and a 1Gb Ethernet connection to provide external access. This NUC is
    known as @tt{nuc1} and is the resource allocated to use the radio.
  }
  @item{
    One @italic{receive-only} NI
    @link["https://www.ettus.com/all-products/ub210-kit/"]{B210} SDR
    connected to a
    @link["https://cdn.taoglas.com/datasheets/GSA.8841.A.105111.pdf"]{Taoglas GSA.8841 I-bar}
    antenna via the channel 'A' RX2 port.
    The B210 is USB3 connected to the same NUC (@tt{nuc1}) as the
    receive-only B210.
  }
  @item{
    One @italic{transmit and receive} NI
    @link["https://www.ettus.com/all-products/ub210-kit/"]{B210} SDR
    connected to a
    @link["https://cdn.taoglas.com/datasheets/GSA.8841.A.105111.pdf"]{Taoglas GSA.8841 I-bar}
    antenna via the channel 'A' TX/RX port.
    The B210 is USB3 connected to a NUC to provide basic compute capability
    and a 1Gb Ethernet connection to provide external access. This NUC is
    known as @tt{nuc2} and is the resource allocated to use the radio.
  }
]

Both NUC compute nodes are:

    @(nodetype "nuc8559" 4 "Coffee Lake, 4 cores"
        (list "CPU"   "Core i7-8559U processor (4 cores, 2.7Ghz)")
	(list "RAM"   "32GB Memory (2 x 16GB DDR4 DIMMs)")
	(list "Disks" "250GB NVMe SSD Drive")
	(list "NIC"   "1GbE embedded NIC")
    )

@section[#:tag "powder-me-hw"]{Mobile-endpoint resources}

@(tb) has twenty "mobile endpoint" (ME) installations which are enclosures
mounted on a rear shelf inside of campus shuttle buses.

As with MEs, each @(tb) FE enclosure contains radio equipment and an
associated small form factor compute nodes and they use commercial
cellular and Campus WiFi to provide seamless access to resources.
However unlike FEs, not all MEs are likely to be available at any time.
Only shuttles that are running and on a route are available for allocation.
For a map of the bus routes and their proximate @(tb) resources see the
@link["https://www.powderwireless.net/powder-map.php"]{Powder Map}.

There are two allocatable devices on each mobile endpoint:
@itemlist[
  @item{
    One Quectel
    @link["https://www.quectel.com/product/5g-rm520n-series/"]{RM520N COTS UE}
    connected to a four-port
    @link["https://www.taoglas.com/datasheets/MA963.A.BIVW.002.pdf"]{Taoglas MA963}
    antenna.
    The UE is USB3 connected to a Supermicro small form factor node to
    provide basic compute capability
    and a 1Gb Ethernet connection to provide external access. This node is
    known as @tt{ed1} and is the resource allocated to use the radio.
  }
  @item{
    One NI
    @link["https://www.ettus.com/all-products/ub210-kit/"]{B210} SDR
    connected to a
    @link["https://cdn.taoglas.com/datasheets/GSA.8841.A.105111.pdf"]{Taoglas GSA.8841 I-bar}
    antenna via the channel 'A' TX/RX port.
    The B210 is likewise USB3 connected to the Supermicro node @tt{ed1}.
  }
]

The
@link["https://www.supermicro.com/en/products/system/Mini-ITX/SYS-E300-8D.cfm"]{Supermicro Compute Node} is:

@(nodetype "e300-8d" 20 "Broadwell, 4 cores"
   (list "CPU"   "Intel Xeon processor D-1518 SoC (4-core, 2.2GHz)")
   (list "RAM"   "64GB Memory (2 x 32GB Hynix 2667MHz DDR4 DIMMs)")
   (list "Disks" "480GB Intel SSDSCKKB480G8 SATA SSD Drive")
   (list "NIC"   "2 x 10Gb Intel SoC Ethernet")
)

@section[#:tag "powder-ne-hw"]{Near-edge computing resources}

As previously documented, all base stations are connected via fiber to
the Fort Douglas datacenter. The fibers are connected to a collection of
CWDM muxes (one per base station) which break out to 1Gb/10Gb connections
to a set of three
@link["https://www.delltechnologies.com/asset/en-in/products/networking/technical-support/dell_emc_networking-s5200_on_spec_sheet.pdf"]{Dell S5248F-ON}
Ethernet switches.
The three switches are interconnected via 2 x 100Gb links and all host
10Gb connections from the rooftop and dense base-stations. One switch also
serves as the aggregation switch with 100Gb uplinks to the MEB and DDC
datacenters that contain further Powder resources and uplinks to Emulab
and CloudLab.

There are 19 servers dedicated to @(tb) use:

@(nodetype "d740" 16 "Skylake, 24 cores"
    (list "CPU"   "2 x Xeon Gold 6126 processors (12 cores, 2.6Ghz)")
    (list "RAM"   "96GB Memory (12 x 8GB RDIMMs, 2.67MT/s)")
    (list "Disks" "2 x 240GB SATA 6Gbps SSD Drives")
    (list "NIC"   "10GbE Dual port embedded NIC (Intel X710)")
    (list "NIC"   "10GbE Dual port converged NIC (Intel X710)")
)

@(nodetype "d840" 3 "Skylake, 64 cores"
    (list "CPU"   "4 x Xeon Gold 6130 processors (16 cores, 2.1Ghz)")
    (list "RAM"   "768GB Memory (24 x 32GB RDIMMs, 2.67MT/s)")
    (list "Disks" "240GB SATA 6Gbps SSD Drive")
    (list "Disks" "4 x 1.6TB NVMe SSD Drive")
    (list "NIC"   "10GbE Dual port embedded NIC (Intel X710)")
    (list "NIC"   "40GbE Dual port converged NIC (Intel XL710)")
)

All nodes are connected to two networks:

@itemlist[
    @item{A 10 Gbps @italic{Ethernet} @bold{``control network''}---this network
        is used for remote access, experiment management, etc., and is
        connected to the public Internet. When you log in to nodes in your
        experiment using @code{ssh}, this is the network you are using.
        @italic{You should not use this network as part of the experiments you
        run in Powder.}
    }

    @item{A 10/40 Gbps @italic{Ethernet} @bold{``experimental network''}.
        Each @code{d740} node has two 10Gb interfaces, one connected to each
        of two Dell S5248F-ON datacenter switches.
        Each @code{d840} node has two 40Gb interfaces, both connected to the
        switch which also hosts the corresponding mMIMO base station
	connections.
    }
]

@section[#:tag "powder-cl-hw"]{Cloud computing resources}

In addition,
@(tb) can allocate bare-metal computing resources on any one of several
federated clusters, including @link["https://www.cloudlab.us"]{CloudLab}
and @link["https://www.emulab.net"]{Emulab}. The closest (latency-wise)
and most plentiful cloud nodes are the Emulab @tt{d430} nodes:

@(nodetype "d430" 160 "Haswell, 16 core, 3 disks"
    (list "CPU" "Two Intel E5-2630v3 8-Core CPUs at 2.4 GHz (Haswell)")
    (list "RAM"  "64GB ECC Memory (8x 8 GB DDR4 2133MT/s)")
    (list "Disk" "One 200 GB 6G SATA SSD")
    (list "Disk" "Two 1 TB 7.2K RPM 6G SATA HDDs")
    (list "NIC"  "Two or four Intel I350 1GbE NICs")
    (list "NIC"  "Two or four Intel X710 10GbE NICs"))

which have multiple 10Gb Ethernet interfaces and 80Gbs connectivity to the
@(tb) switch fabric.
