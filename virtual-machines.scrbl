#lang scribble/manual

@(require "defs.rkt")

@title[#:tag "virtual-machines-advanced" #:style main-style #:version apt-version]{Virtual Machines}

A @(tb) virtual node is a virtual machine running on top of
a regular operating system. @(tb) virtual nodes are based on the
@seclink["xen-virtual-machines"]{Xen hypervisor}, which
allows groups of processes to be isolated from each other
while running on the same physical machine. @(tb) virtual nodes provide
isolation of the filesystem, process, network, and account
namespaces. Thus, each virtual node has its own private filesystem,
process hierarchy, network interfaces and IP addresses, and set of users
and groups. This level of virtualization allows unmodified applications
to run as though they were on a real machine. Virtual network interfaces
support an arbitrary number of virtual network links. These links may be
individually shaped according to user-specified link parameters, and may
be multiplexed over physical links or used to connect to virtual nodes
within a single physical node.

There are a few specific differences between virtual and physical nodes.
First, @(tb) physical nodes have a routable, public IPv4 address
allowing direct remote access (unless the @(tb) installation has been
configured to use unroutable control network IP addresses, which is very
rare).  However, virtual nodes are assigned control network IP addresses
on a private network (typically the @tt{172.16/12} subnet) and are
remotely accessible over ssh via DNAT (destination network-address
translation) to the physical host's public control network IP address,
to a high-numbered port.  Depending on local configuration, it may be
possible to @seclink["public-ip-access"]{request routable IP addresses}
for specific virtual nodes to enable direct remote access.  Note that
virtual nodes are always able to access the public Internet via SNAT
(source network-address translation; nearly identical to masquerading).

Second, virtual nodes and their virtual network interfaces are connected
by virtual links built atop physical links and physical interfaces.  The
virtualization of a physical device/link decreases the fidelity of the
network emulation.  Moreover, several virtual links may share the same
physical links via multiplexing.  Individual links are isolated at layer
2, but they are not isolated in terms of performance.  If you request a
specific bandwidth for a given set of links, our resource mapper will
ensure that if multiple virtual links are mapped to a single physical
link, the sum of the bandwidths of the virtual links will not exceed the
capacity of the physical link (unless you also specify that this
constraint can be ignored by setting the @tt{best_effort} link parameter to
@tt{True}).  For example, no more than ten 1Gbps virtual links can be
mapped to a 10Gbps physical link.

Finally, when you allocate virtual nodes, you can specify the amount of
CPU and RAM (and, for Xen VMs, virtual disk space) each node will be
allocated.  @(tb)'s resource assigner will not oversubscribe these quantities.


@section[#:tag "xen-virtual-machines"]{Xen VMs}

These examples show the basics of allocating Xen VMs:
@seclink["geni-lib-example-single-vm"]{a single Xen VM node},
@seclink["geni-lib-example-two-vm-lan"]{two Xen VMs in a LAN},
@seclink["geni-lib-example-single-vm-sized"]{a Xen VM with custom disk size}.
In the sections below, we discuss advanced Xen VM allocation features.

@subsection[#:tag "xen-cores-ram"]{Controlling CPU and Memory}

You can control the number of cores and the amount of memory allocated
to each VM by setting the @tt{cores} and @tt{ram} instance variables of
a @tt{XenVM} object, as shown in the following example:

@code-sample["geni-lib-xen-cores-ram.py"]

@subsection[#:tag "xen-extrafs"]{Controlling Disk Space}

Each Xen VM is given enough disk space to hold the requested image.
Most @(tb) images are built with a 16 GB root partition, typically with
about 25% of the disk space used by the operating system.  If the
remaining space is not enough for your needs, you can request additional
disk space by setting a @tt{XEN_EXTRAFS} node attribute, as shown in the
following example.

@code-sample["geni-lib-xen-extrafs.py"]

This attribute's unit is in GB.  As with @(tb) physical nodes, the extra
disk space will appear in the fourth partition of your VM's disk. You can
turn this extra space into a usable file system by logging into your
VM and doing:

@codeblock{
mynode> sudo mkdir /dirname
mynode> sudo /usr/local/etc/emulab/mkextrafs.pl /dirname
}

where @tt{dirname} is the directory you want your newly-formatted file
system to be mounted.

@code-sample["geni-lib-xen-cores-ram.py"]

@subsection[#:tag "xen-hvm"]{Setting HVM Mode}

By default, all Xen VMs are @hyperlink["https://wiki.xen.org/wiki/Paravirtualization_(PV)"]{paravirtualized}.
If you need @hyperlink["https://wiki.xen.org/wiki/Xen_Project_Software_Overview#HVM_and_its_variants_.28x86.29"]{hardware virtualization}
instead, you must set a @tt{XEN_FORCE_HVM} node attribute, as shown in
this example:

@code-sample["geni-lib-single-hvm.py"]

You can set this attribute only for dedicated-mode VMs.  Shared VMs are
available only in paravirtualized mode.

@subsection[#:tag "xen-shared-mode"]{Dedicated and Shared VMs}

In @(tb), Xen VMs can be created in @italic{dedicated} or
@italic{shared} mode.  In dedicated mode, VMs run on physical
nodes that are reserved to a particular experiment, and you have
root-level access to the underlying physical machine.  In shared mode,
VMs run on physical machines that host VMs from
potentially many experiments, and users do not have access to the
underlying physical machine.

To request that a VM run in dedicated mode, set its @tt{exclusive} attribute
to @tt{True}; for example @tt{node.exclusive = True}. To run it in shared mode,
set this value to @tt{False}.
